from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import json
import requests
from django.http import JsonResponse
from .models import Data
from django.contrib.auth import authenticate, login, logout



def books(request, keyword = "quilting"):
    linkJson = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + keyword)
    parse = json.dumps(linkJson.json())
    return HttpResponse(parse)

def home(request):
    return render(request, "book.html")

def fungsi_hello(request):
    if request.method == 'POST':
        tgl = request.POST['tgl']
        cat = request.POST['cat']
        json = Data(tgl= tgl,cat=cat)
        json.save()
        return JsonResponse(json.as_dict())

def valid(request):
    tgl = request.POST.get('tgl', None)
    cat = request.POST.get('cat', None)
    data = {
        'is_taken': Data.objects.filter(tgl=tgl,cat=cat).exists()
    }
    return JsonResponse(data)

def login(request):
    return render(request,'login.html')

def logout_view(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/searchbook')
