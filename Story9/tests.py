from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Story8UnitTest(TestCase):

    def test_story9_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story9_home_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code,200)

    def test_story9_home_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, home)

    def test_story9_login_func(self):
        found = resolve('/')
        self.assertEqual(found.func, login)

    def test_template_story9(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login.html')

    def test_template_home_story9(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'book.html')

    def test_landing_page_(self):
         response = Client().get('/home/')
         html_response = response.content.decode('utf-8')
         self.assertIn('List of Books',html_response)

    def test_landing_page_login(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Sign In',html_response)