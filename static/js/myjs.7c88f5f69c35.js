// Wrap every letter in a span
$('.ml1 .letters').each(function(){
  $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
});

anime.timeline({loop: true})
  .add({
    targets: '.ml1 .letter',
    scale: [0.3,1],
    opacity: [0,1],
    translateZ: 0,
    easing: "easeOutExpo",
    duration: 600,
    delay: function(el, i) {
      return 70 * (i+1)
    }
  }).add({
    targets: '.ml1 .line',
    scaleX: [0,1],
    opacity: [0.5,1],
    easing: "easeOutExpo",
    duration: 700,
    offset: '-=875',
    delay: function(el, i, l) {
      return 80 * (l - i);
    }
  }).add({
    targets: '.ml1',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
  });

$.ajax({
    url: "{% url 'book' %}",
    datatype: 'json',
    success: function(result){
        console.log("test");
        var books_object = jQuery.parseJSON(result);
        console.log(books_object);
        renderHTML(books_object);
    }
  });

function renderHTML(data){
    var books_list = data.items;
          for (var i = 0; i < books_list.length; i++){
            var title = books_list[i].volumeInfo.title;
            var author = books_list[i].volumeInfo.authors[0];
            var published = books_list[i].volumeInfo.publishedDate;
            var temp = title.replace(/'/g, "\\'");
            var table =
            '<tr class="table-books">'+
            '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
            '<td class="title">'+title+'</td>'+
            '<td class= "author">'+author+'</td>'+
            '<td class= "published">' + published+'</td>'+
            '<td class= "favourite">' + '<button class="btnnn" onClick="addToFavourite(\''+temp+'\')"><i class="far fa-star"></i></button>' + '</td>';
            $('tbody').append(table);
          }
  }